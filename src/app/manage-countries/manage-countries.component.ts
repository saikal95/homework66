import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {Country} from "../shared/country.model";
import {CountryService} from "../shared/country.service";

@Component({
  selector: 'app-manage-countries',
  templateUrl: './manage-countries.component.html',
  styleUrls: ['./manage-countries.component.css']
})
export class ManageCountriesComponent implements OnInit, OnDestroy {
  countries: Country [] = [];
  countriesChangeSubscription! : Subscription;
  countriesFetchingSubscription! : Subscription;
  isFetching = false;

  constructor(private countryService: CountryService) { }

  ngOnInit(): void {
    this.countries = this.countryService.getCountries();
    this.countriesChangeSubscription = this.countryService.countriesChange.subscribe((countries:Country[])=> {
      this.countries = countries;
    });
    this.countriesFetchingSubscription =  this.countryService.countriesFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    })
    this.countryService.fetchCountries();
  }

  ngOnDestroy() {
    this.countriesChangeSubscription.unsubscribe();
    this.countriesFetchingSubscription.unsubscribe();
  }

}
