import { Injectable } from '@angular/core';
import {CountryService} from "../shared/country.service";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs";
import {Country} from "../shared/country.model";

@Injectable({
  providedIn: 'root'
})
export class CountrResolverService implements Resolve<Country>{

  constructor(private countryService : CountryService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Country>  {

    const countryId = <string>route.params['alpha3Code'];
    return this.countryService.fetchCountry(countryId);

  }




}
