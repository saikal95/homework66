import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CountrResolverService} from "./countries/countr-resolver.service";
import {CountryDetailsComponent} from "./country-details/country-details.component";

const routes: Routes = [
  {path: 'countries/:alpha3Code', component: CountryDetailsComponent,
    resolve:{
      country: CountrResolverService
    }},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


