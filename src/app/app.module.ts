import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ManageCountriesComponent } from './manage-countries/manage-countries.component';
import { CountryDetailsComponent } from './country-details/country-details.component';
import { CountriesComponent } from './countries/countries.component';
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {CountryService} from "./shared/country.service";

@NgModule({
  declarations: [
    AppComponent,
    ManageCountriesComponent,
    CountryDetailsComponent,
    CountriesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [CountryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
