export class Country {
  constructor(
    public name: string,
    public capital: string,
    public alpha3Code: string,
    public region: string,
    public flag: string,
  ) {}
}
