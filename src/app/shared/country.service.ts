import {Injectable} from "@angular/core";
import {Subject} from "rxjs";
import {Country} from "./country.model";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";


@Injectable()
export class CountryService {

  countriesChange = new Subject <Country[]>();
  countriesFetching = new Subject<boolean>();
  countriesUploading = new Subject <boolean>();


  private countries: Country[] = [];


  constructor(private http : HttpClient) {}

  getCountries(){
    return this.countries.slice();
   }

  fetchCountries(){
    this.countriesFetching.next(true);
    this.http.get<{[id:string]:Country}>('http://146.185.154.90:8080/restcountries/rest/v2/all?fields=name;alpha3Code')
      .pipe(map(result => {
        return Object.keys(result).map(id=> {
          const countryData = result[id];
          return new Country(
            countryData.name,
            countryData.capital,
            countryData.alpha3Code,
            countryData.region,
            countryData.flag
          );
        });
      }))
      .subscribe(countries=>{
        this.countries = countries;
        this.countriesChange.next(this.countries.slice());
        this.countriesFetching.next(false);
      }, () => {
        this.countriesFetching.next(false);
      })
  }

  fetchCountry(alpha3Code:string){
    return this.http.get<Country>(`http://146.185.154.90:8080/restcountries/rest/v2/alpha/${alpha3Code}`).pipe(map(
      result => {
        return new Country(
          result.name,result.capital,result.alpha3Code,result.region,result.flag);
      }
    ))
  }

  }
